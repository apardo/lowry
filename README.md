Set up a testing environment:
```
make deps
make fixtures
```

Build lowry and run it with the demo data:
```
make all
make demo
```


Now go to your browser, open http://localhost:8080 and you can login as:

* Usuaria: user
* Contraseña: foobar

Or as admin:
* Usuaria: superuser
* Contraseña: foobar


# tests

The tests needs a clean ldap with the `examples/data.ldif` added.

You can run them:
```
make test
```
