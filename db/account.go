package db

import (
	"time"
)

var (
	accountBucket = []byte("account")
)

type account struct {
	Host         string
	CreationDate time.Time
}

// AddAccount stores in the db the account and it's host who invited it
func (db *DB) AddAccount(user string, host string) error {
	return db.put(accountBucket, user, account{host, time.Now()})
}

// GetHost that invited the user
func (db *DB) GetHost(user string) (string, error) {
	var acc account
	err := db.get(accountBucket, user, &acc)
	return acc.Host, err
}

// ExpireAccounts older than duration
func (db *DB) ExpireAccounts(duration time.Duration) error {
	return db.expire(accountBucket, duration)
}
