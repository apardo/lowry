package db

var (
	askSindominanteBucket = []byte("ask-sindominante")
)

// HasBeingAskedForSindominante checks if the user has being already asked for sindominante
func (db *DB) HasBeingAskedForSindominante(user string) bool {
	var answer string
	err := db.get(askSindominanteBucket, user, &answer)
	return err == nil
}

// AnswerForSindominante register the answer for sindominante for user
func (db *DB) AnswerForSindominante(user string, answer string) error {
	return db.put(askSindominanteBucket, user, answer)
}
