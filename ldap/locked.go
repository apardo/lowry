package ldap

import (
	"log"
	"strings"
)

type Locked int

const (
	Unlocked Locked = iota
	Unused
	Unknown
)

func LockedFromString(s string) Locked {
	switch strings.ToLower(s) {
	case "":
		return Unlocked
	case "unused":
		return Unused
	default:
		log.Printf("Not valid locked status: %s", s)
		return Unknown
	}
}

func (r Locked) String() string {
	switch r {
	case Unlocked:
		return ""
	case Unused:
		return "unused"
	default:
		return "unknown"
	}
}
