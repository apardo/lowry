package ldap

import "testing"

const (
	admin = "superuser"
	group = "adm"
)

func TestInGroup(t *testing.T) {
	l := testLdap(t)
	if !l.InGroup(admin, group) {
		t.Errorf("%s should be part of group %s", admin, group)
	}

	if l.InGroup(user, group) {
		t.Errorf("%s should not be part of group %s", user, group)
	}

	if l.InGroup(admin, "nonexistinggroup") {
		t.Errorf("%s should not be part of nonexistinggroup", admin)
	}
}

func TestGetGroup(t *testing.T) {
	l := testLdap(t)

	group, err := l.GetGroup("adm")
	if err != nil {
		t.Fatalf("GetGroup(adm) failed: %v", err)
	}
	if group.Name != "adm" {
		t.Errorf("Wrong group name not in adm: %v", group)
	}
	if group.Members[0] != "superuser" {
		t.Errorf("superuser not part of adm: %v", group)
	}
	if group.GID != 1 {
		t.Errorf("Wrong GID in adm: %v", group)
	}
}

func TestGetGID(t *testing.T) {
	l := testLdap(t)

	group, err := l.GetGID(1)
	if err != nil {
		t.Fatalf("GetGID(1) failed: %v", err)
	}
	if group.Name != "adm" {
		t.Errorf("Wrong group name not in adm: %v", group)
	}
	if group.Members[0] != "superuser" {
		t.Errorf("superuser not part of adm: %v", group)
	}
	if group.GID != 1 {
		t.Errorf("Wrong GID in adm: %v", group)
	}
}

func TestListGroups(t *testing.T) {
	l := testLdap(t)

	groups, err := l.ListGroups()
	if err != nil {
		t.Errorf("ListGroups() failed: %v", err)
	}
	if len(groups) != 1 {
		t.Fatalf("Wrong number of groups: %v", groups)
	}
	if groups[0].Name != "adm" {
		t.Errorf("Wrong group name not in adm: %v", groups)
	}
}

func TestUserGroups(t *testing.T) {
	l := testLdap(t)

	groups, err := l.UserGroups("user")
	if err != nil {
		t.Errorf("UserGroups(\"user\") failed: %v", err)
	}
	if len(groups) != 0 {
		t.Errorf("user is member of some groups: %v", groups)
	}

	groups, err = l.UserGroups("superuser")
	if err != nil {
		t.Errorf("UserGroups(\"superuser\") failed: %v", err)
	}
	if len(groups) != 1 {
		t.Fatalf("superuser grong number of groups: %v", groups)
	}
	if groups[0].Name != "adm" {
		t.Errorf("superuser not in adm: %v", groups)
	}
}

func TestAddDelGroups(t *testing.T) {
	const name = "test"

	l := testLdap(t)

	_, err := l.GetGroup(name)
	if err == nil {
		t.Errorf("group %s allready exist", name)
	}

	err = l.AddGroup(name)
	if err != nil {
		t.Fatalf("CreateGroup(\"%s\") failed: %v", name, err)
	}
	_, err = l.GetGroup(name)
	if err != nil {
		t.Errorf("GetGroup(\"%s\") failed: %v", name, err)
	}
	err = l.DelGroup(name)
	if err != nil {
		t.Errorf("DeleteGroup(\"%s\") failed: %v", name, err)
	}
}

func TestAddExistingGroup(t *testing.T) {
	l := testLdap(t)

	err := l.AddGroup("adm")
	if err == nil {
		t.Errorf("Create group 'adm' didn't fail")
	}

}

func TestAddUserGroup(t *testing.T) {
	l := testLdap(t)
	if l.InGroup(user, group) {
		t.Errorf("%s should not be part of group %s", user, group)
	}

	err := l.AddUserGroup(user, group)
	if err != nil {
		t.Errorf("AddUserGroup(%s, %s) failed: %v", user, group, err)
	}
	if !l.InGroup(user, group) {
		t.Errorf("%s should be now part of group %s", user, group)
	}
	if !l.InGroup(admin, group) {
		t.Errorf("%s should still be part of group %s", admin, group)
	}

	err = l.DelUserGroup(user, group)
	if err != nil {
		t.Errorf("DelUserGroup(%s, %s) failed: %v", user, group, err)
	}
	if l.InGroup(user, group) {
		t.Errorf("%s should not be part of group %s", user, group)
	}
	if !l.InGroup(admin, group) {
		t.Errorf("%s should still be part of group %s", admin, group)
	}
}
