package server

import (
	"log"
	"net/http"

	"0xacab.org/sindominio/lowry/ldap"
)

func (s *server) homeHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("index", w, r)
	if response.User == "" {
		response = s.newResponse("login", w, r)
		response.execute(false)
		return
	}

	askSindominante := s.needsToAskForSindominante(response.User)
	needPasswordChange := !s.ldap.IsUserPassUptodate(response.User)
	data := struct {
		AskSindominante    bool
		NeedPasswordChange bool
	}{askSindominante, needPasswordChange}
	response.execute(data)
}

func (s *server) loginHandler(w http.ResponseWriter, r *http.Request) {
	user := r.FormValue("user")
	pass := r.FormValue("password")

	err := s.ldap.ValidateUser(user, pass)
	if err != nil {
		response := s.newResponse("login", w, r)
		response.execute(true)
		return
	}

	s.sess.set(user, w, r)
	http.Redirect(w, r, "/", http.StatusFound)
}

func (s *server) logoutHandler(w http.ResponseWriter, r *http.Request) {
	s.sess.del(w, r)
	http.Redirect(w, r, "/", http.StatusFound)
}

func (s *server) passwordHandler(w http.ResponseWriter, r *http.Request) {
	response := s.newResponse("password", w, r)
	if response.User == "" {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	if r.Method != "POST" {
		response.execute("")
		return
	}

	oldpass := r.FormValue("oldpass")
	pass := r.FormValue("password")
	pass2 := r.FormValue("password2")

	if pass != pass2 {
		response.execute("WrongPass")
		return
	}
	if pass == oldpass {
		response.execute("SameOldPass")
		return
	}

	err := s.ldap.ChangePass(response.User, oldpass, pass)
	if err != nil {
		response.execute("WrongOldpass")
	} else {

		response.execute("PassChanged")
	}
}

func (s *server) askSindominante(w http.ResponseWriter, r *http.Request) {
	role := r.FormValue("role")

	response := s.newResponse("", w, r)
	if !s.userInAskList(response.User) {
		log.Printf("An attempt to set role without being in the list: %s - %s", response.User, role)
		s.forbiddenHandler(w, r)
		return
	}

	err := s.ldap.ChangeRole(response.User, ldap.RoleFromString(role))
	if err != nil {
		log.Printf("An error has ocurred setting the role: %v", err)
	} else {
		s.db.AnswerForSindominante(response.User, role)
	}
	http.Redirect(w, r, "/", http.StatusFound)
}

func (s *server) needsToAskForSindominante(user string) bool {
	if !s.userInAskList(user) {
		return false
	}

	return !s.db.HasBeingAskedForSindominante(user)
}

func (s *server) userInAskList(user string) bool {
	for _, u := range s.usersAskRole {
		if u == user {
			return true
		}
	}
	return false
}
